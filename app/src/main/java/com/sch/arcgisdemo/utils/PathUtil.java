
package com.sch.arcgisdemo.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.sch.arcgisdemo.R;

import java.io.File;

/**
 * 文件目录获取通用类
 */
public class PathUtil {

    /**
     * 获取SD卡根目录
     *
     * @return
     */
    public static String getSDPath() {
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();// 获取根目录
            return sdDir.toString();
        } else {
            return "-1";
        }


    }

    /**
     * 获取应用程序数据包根目录
     *
     * @return
     */
    public static String getAppDirPath(Context context) {
        String _SDPath = getSDPath();
        if (_SDPath.equals("-1")) {
            Toast.makeText(context, "获取SD卡路径失败！", Toast.LENGTH_LONG);
            return "-1";
        }
        String dirPathString = _SDPath + "/" + context.getString(R.string.app_name);
        File file = new File(dirPathString);
        if (!file.exists()) {
            file.mkdir();
        }
        return dirPathString;
    }

    /**
     * 获取图片根目录
     *
     * @return
     */
    public static String getPicDirPath(Context context) {
        String picDirPathString = getAppDirPath(context) + "/UploadPic";
        File file = new File(picDirPathString);
        if (!file.exists()) {
            file.mkdir();
        }
        return picDirPathString;
    }

    /**
     * 获取本地管线地图数据目录
     *
     * @return
     */
    public static String getLocalGXGeodatabasePath(Context context) {
        String localMapDirPathString = getAppDirPath(context) + "/Geodatabase";
        File file = new File(localMapDirPathString);
        if (!file.exists()) {
            file.mkdir();
        }
        Log.i("本地管线地图数据目录", localMapDirPathString);
        return localMapDirPathString;
    }

}