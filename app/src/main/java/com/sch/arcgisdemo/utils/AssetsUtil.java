package com.sch.arcgisdemo.utils;

/**
 * Created by Sch.
 * Date: 2020/7/15
 * description:
 */

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by sgll on 2019/1/8.
 * Assets读取文件工具类
 */
public class AssetsUtil {

    /**
     * 将assets下的文件放到sd指定目录下
     *
     * @param context    上下文
     * @param assetsName assets下的文件
     * @param sdCardPath sd卡的路径
     */
    public static void putAssetsToSDCard(Context context, String assetsName, String sdCardPath) {
        AssetManager assetManager = context.getAssets();
        try {
            InputStream is = assetManager.open(assetsName);
            byte[] mByte = new byte[1024];
            int bt = 0;
            File file = new File(sdCardPath + File.separator
                    + assetsName);
            if (!file.exists()) {
                Log.i("AssetsUtil", "创建文件");
                // 创建文件
                file.createNewFile();
            } else {
                //已经存在直接退出
                Log.i("AssetsUtil", "文件已经存在");
                return;
            }
            // 写入流
            FileOutputStream fos = new FileOutputStream(file);
            // assets为文件,从文件中读取流
            while ((bt = is.read(mByte)) != -1) {
                // 写入流到文件中
                fos.write(mByte, 0, bt);
            }
            // 刷新缓冲区
            fos.flush();
            // 关闭读取流
            is.close();
            // 关闭写入流
            fos.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
