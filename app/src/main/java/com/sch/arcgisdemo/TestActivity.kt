package com.sch.arcgisdemo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import cn.sddman.arcgistool.layer.TianDiTuLayer
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.internal.jni.it
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.Basemap
import com.esri.arcgisruntime.mapping.view.LocationDisplay
import com.sch.arcgisdemo.map.AMapTiledLayerClass
import com.sch.arcgisdemo.map.TianDiTuTiledLayerClass
import kotlinx.android.synthetic.main.activity_test.*


/**
 * Created by Sch.
 * Date: 2020/7/16
 * description:
 */
class TestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

    }

}