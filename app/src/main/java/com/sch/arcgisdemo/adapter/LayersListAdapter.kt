package com.sch.arcgisdemo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.esri.arcgisruntime.layers.Layer
import com.esri.arcgisruntime.mapping.LayerList
import com.sch.arcgisdemo.R

/**
 * Created by Sch.
 * Date: 2020/7/14
 * description:
 */
class LayersListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val list by lazy { ArrayList<Layer>() }

    fun updata(list: ArrayList<Layer>) {
        list?.let {
            this.list.clear()
            this.list.addAll(it)
            notifyDataSetChanged()
        }
    }

    class LayerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cbName: CheckBox = itemView.findViewById(R.id.cbLayerName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layers, parent, false)
        return LayerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val layer = list[position]
        holder as LayerViewHolder
        holder.cbName.text = layer.name
        holder.cbName.isChecked = layer.isVisible
        holder.cbName.setOnCheckedChangeListener { compoundButton, b ->
            layer.isVisible = b
        }
    }

}