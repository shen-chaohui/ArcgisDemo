package com.sch.arcgisdemo;

import android.app.Application;

/**
 * Created by Sch.
 * Date: 2019/12/13
 * description:
 */
public class MyApplication extends Application {
    private static MyApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static MyApplication getInstance() {
        return sInstance;
    }

}
