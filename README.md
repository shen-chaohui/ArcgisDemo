# ArcgisDemo 基础功能展示记录
##  首页
![首页](picture/image.png)
## 底图切换功能
![地图类别切换](picture/image_1.png)
## 定位到某个位置
![定位到某个位置](picture/image_2.png)
## 添加点、线.、面
![image_3](picture/image_3.png)
![image_4](picture/image_4.png)
![image_5](picture/image_5.png)
## 添加本地geodatabase
上边提示文字忘了改了，忽略吧
![image_6](picture/image_6.png)
## 点击查询
![image_7](picture/image_7.png)
## 拉框查询
![image_8](picture/image_8.png)
## 地图功能
![image_9](picture/image_9.png)
指南针。图层控制。清除地图上绘制的graphic。。
## 坐标转换问题
![image_10](picture/image_10.png)
第一是定位直接获取到的坐标 locationDisplay?.mapLocation
第二个是定位获取到的gps坐标 locationDisplay?.location?.position
第三个是地图坐标系通过坐标转换得到的wgs84坐标，第二个跟第三个一样，说明转换无误。